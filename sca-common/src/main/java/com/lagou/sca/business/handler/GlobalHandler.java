package com.lagou.sca.business.handler;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.lagou.sca.common.pojo.ResultInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class GlobalHandler implements BlockExceptionHandler {

    public static ResultInfo<Object> handleException(BlockException blockException) {
        return ResultInfo.error(blockException.getMessage(), ExceptionUtil.stacktraceToString(blockException));
    }

    public static ResultInfo<Object> handleError() {
        return ResultInfo.error("服务出错啦，请稍后重试！");
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       BlockException e) throws Exception {
        log.warn(">>>>>限流降级统一处理:"+e.getMessage());
        ResultInfo error = ResultInfo.error("服务出错啦，请稍后重试！", "限流降级处理！");
        response.setHeader("content-type", "application/json;charset=UTF-8");
        response.getWriter().write(JSONUtil.toJsonStr(error.setCode(0000)));
    }
}
