package com.lagou.sca.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lagou.sca.business.model.User;
import com.lagou.sca.common.pojo.ResultInfo;
import com.nimbusds.jose.JOSEException;

import java.text.ParseException;

public interface UserService extends IService<User> {

    ResultInfo<Boolean> register(User user);

    ResultInfo<String> login(String email, String password) throws JOSEException;

    ResultInfo<Boolean> isRegistered(String email);

    ResultInfo<String> info(String token) throws ParseException, JOSEException;

    User one(String userId);

    boolean add(User user);
}
