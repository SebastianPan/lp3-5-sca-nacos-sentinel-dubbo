package com.lagou.sca.business.service;

import com.lagou.sca.common.pojo.ResultInfo;

public interface EmailService {

    ResultInfo<Boolean> email(String email, String code);

}
