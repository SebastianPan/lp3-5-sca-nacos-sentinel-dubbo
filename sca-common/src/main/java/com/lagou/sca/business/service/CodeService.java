package com.lagou.sca.business.service;

import com.lagou.sca.common.pojo.ResultInfo;

public interface CodeService {

    ResultInfo<Boolean> create(String email);

    ResultInfo<String> validate(String email, String code);

}
