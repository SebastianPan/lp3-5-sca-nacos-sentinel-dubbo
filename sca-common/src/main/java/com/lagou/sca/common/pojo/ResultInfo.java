package com.lagou.sca.common.pojo;

import com.lagou.sca.common.enums.ResultCode;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ResultInfo<T> implements Serializable {

    private Integer code;

    private String message;

    private T data;

    private String detail;

    private Boolean success;

    public Boolean isSuccess(){
        return success;
    }


    /**
     * 默认 的ResultInfo对象 msg = 成功
     *
     * @param data 默认值
     * @return
     */
    public static <T> ResultInfo<T> success(T data) {
        ResultInfo<T> resultInfo = new ResultInfo<T>();
        resultInfo.setCode(ResultCode.SUCCESS.getCode());
        resultInfo.setData(data);
        resultInfo.setMessage("成功");
        resultInfo.setSuccess(true);
        return resultInfo;
    }

    /**
     * 默认 的ResultInfo对象 msg = 成功
     *
     * @param data 默认值
     * @return
     */
    public static <T> ResultInfo<T> success(T data, String msg) {
        ResultInfo<T> resultInfo = new ResultInfo<T>();
        resultInfo.setCode(ResultCode.SUCCESS.getCode());
        resultInfo.setData(data);
        resultInfo.setMessage(msg);
        resultInfo.setSuccess(true);
        return resultInfo;
    }


    public static ResultInfo error(String msg) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(ResultCode.ERROR.getCode());
        resultInfo.setSuccess(false);
        resultInfo.setMessage(msg);
        return resultInfo;
    }

    public static ResultInfo error(String msg, String detail) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(ResultCode.ERROR.getCode());
        resultInfo.setSuccess(false);
        resultInfo.setMessage(msg);
        resultInfo.setDetail(detail);
        return resultInfo;
    }

    public static ResultInfo error(ResultCode code, String msg) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(code.getCode());
        resultInfo.setSuccess(false);
        resultInfo.setMessage(msg);
        return resultInfo;
    }

    public static ResultInfo error(ResultCode code, String msg, String detail) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(code.getCode());
        resultInfo.setSuccess(false);
        resultInfo.setDetail(detail);
        resultInfo.setMessage(msg);
        return resultInfo;
    }

}
