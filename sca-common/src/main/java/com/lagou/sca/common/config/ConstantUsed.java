package com.lagou.sca.common.config;

public class ConstantUsed {

    public static final String JwtTokenSecret = "66zHwE0IKeEVqoyosxcqUiPXsf0iLMlp";
    public static final String PrefixRedisToken = "Token@";
    public static final String PrefixRedisEmailCode = "EmailCode@";
    public static final String Header_Authorization = "Authorization";

}
