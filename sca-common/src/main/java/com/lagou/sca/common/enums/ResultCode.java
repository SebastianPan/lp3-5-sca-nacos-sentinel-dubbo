package com.lagou.sca.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
public enum ResultCode {

    SUCCESS(200, "成功！"),
    ERROR(400, "失败！"),
    UNAUTHORIZED(401,"无访问权限！"),
    MORE_REGISTER(303, "您操作太频繁啦，请求已被拒绝！");


    @EnumValue
    private Integer code;
    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static List<Integer> getCodes(){
        List<Integer> codes = new ArrayList<>();
        Arrays.stream(ResultCode.values()).forEach(single ->{
            codes.add(single.getCode());
        });
        return codes;
    }

    @JsonCreator
    public static ResultCode getItem(String enumx){
        for(ResultCode item : values()){
            if(String.valueOf(item.getCode()).equals(enumx)){
                return item;
            }
            if(item.name().equals(enumx)){
                return item;
            }
        }
        return null;
    }


    }
