package com.lagou.sca.common.plugins.jwt;

import cn.hutool.json.JSONUtil;
import com.lagou.sca.common.pojo.ResultInfo;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;
import org.springframework.stereotype.Component;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
public class JwtTokenService {

    /**
     * 使用HMAC算法生成token
     */
    public ResultInfo<String> generateTokenByHMAC(String payloadStr, String secret) throws JOSEException {
        //创建JWS头，设置签名算法和类型
        JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.HS256).
                type(JOSEObjectType.JWT)
                .build();
        //将负载信息封装到Payload中
        Payload payload = new Payload(payloadStr);
        //创建JWS对象
        JWSObject jwsObject = new JWSObject(jwsHeader, payload);
        //创建HMAC签名器
        JWSSigner jwsSigner = new MACSigner(secret);
        //签名
        jwsObject.sign(jwsSigner);
        return ResultInfo.success(jwsObject.serialize());
    }

    /**
     * 使用HMAC算法校验token
     */
    public ResultInfo<TokenInfo> verifyTokenByHMAC(String token, String secret) throws ParseException, JOSEException {
        //从token中解析JWS对象
        JWSObject jwsObject = JWSObject.parse(token);
        //创建HMAC验证器
        JWSVerifier jwsVerifier = new MACVerifier(secret);
        if (!jwsObject.verify(jwsVerifier)) {
            return ResultInfo.error("token签名不合法！");
        }
        String payload = jwsObject.getPayload().toString();
        TokenInfo tokenInfo = JSONUtil.toBean(payload, TokenInfo.class);
        if (tokenInfo.getExp() < new Date().getTime()) {
            return ResultInfo.error("token已过期！");
        }
        return ResultInfo.success(tokenInfo);
    }

    /**
     * 使用RSA算法生成token
     */
    public ResultInfo<String> generateTokenByRSA(String payloadStr, RSAKey rsaKey) throws JOSEException {
        //创建JWS头，设置签名算法和类型
        JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256)
                .type(JOSEObjectType.JWT)
                .build();
        //将负载信息封装到Payload中
        Payload payload = new Payload(payloadStr);
        //创建JWS对象
        JWSObject jwsObject = new JWSObject(jwsHeader, payload);
        //创建RSA签名器
        JWSSigner jwsSigner = new RSASSASigner(rsaKey, true);
        //签名
        jwsObject.sign(jwsSigner);
        return ResultInfo.success(jwsObject.serialize());
    }

    /**
     * 使用RSA算法校验token
     */
    public ResultInfo<TokenInfo> verifyTokenByRSA(String token, RSAKey rsaKey) throws ParseException, JOSEException {
        //从token中解析JWS对象
        JWSObject jwsObject = JWSObject.parse(token);
        RSAKey publicRsaKey = rsaKey.toPublicJWK();
        //使用RSA公钥创建RSA验证器
        JWSVerifier jwsVerifier = new RSASSAVerifier(publicRsaKey);
        if (!jwsObject.verify(jwsVerifier)) {
            return ResultInfo.error("token签名不合法！");
        }
        String payload = jwsObject.getPayload().toString();
        TokenInfo tokenInfo = JSONUtil.toBean(payload, TokenInfo.class);
        if (tokenInfo.getExp() < new Date().getTime()) {
            return ResultInfo.error("token已过期！");
        }
        return ResultInfo.success(tokenInfo);
    }


    /**
     *
     * @param email
     * @param now new Date()
     * @param exp DateUtil.offsetSecond(now, 60*60)
     * @param username
     * @param roles CollUtil.toList("ADMIN")
     * @return
     */
    public TokenInfo getDefaultPayloadDto(String email, Date now, Date exp, String username, List<String> roles) {
        return TokenInfo.builder()
                .email(email)
                .iat(now.getTime())
                .exp(exp.getTime())
                .jti(UUID.randomUUID().toString())
                .username(username)
                .authorities(roles)
                .build();
    }

    /**
     * 获取默认的RSAKey
     * keytool -genkey -alias jwt -keyalg RSA -keystore jwt.jks
     */
    public RSAKey getDefaultRSAKey() {
        //从classpath下获取RSA秘钥对
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("jwt.jks"), "123456".toCharArray());
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair("jwt", "123456".toCharArray());
        //获取RSA公钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        //获取RSA私钥
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        return new RSAKey.Builder(publicKey).privateKey(privateKey).build();
    }
}