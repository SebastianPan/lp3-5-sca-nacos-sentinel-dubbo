package com.lagou.sca.service.user.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lagou.sca.business.model.User;
import com.lagou.sca.business.service.UserService;
import com.lagou.sca.common.config.ConstantUsed;
import com.lagou.sca.common.plugins.jwt.JwtTokenService;
import com.lagou.sca.common.plugins.jwt.TokenInfo;
import com.lagou.sca.common.pojo.ResultInfo;
import com.lagou.sca.common.utils.RedisUtil;
import com.lagou.sca.service.user.dao.UserDao;
import com.nimbusds.jose.JOSEException;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@DubboService
@org.springframework.stereotype.Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

    @Autowired
    private JwtTokenService jwtTokenService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private UserDao userDao;

    @Override
    public ResultInfo<Boolean> register(User user){
        Integer hasCount = this.count(new QueryWrapper<User>().lambda().eq(User::getMail, user.getMail()));
        if(hasCount > 0){
            return ResultInfo.error("注册邮箱已存在，换一个试试！");
        }
        if(StrUtil.isEmpty(user.getPassword())){
            return ResultInfo.error("请输入密码！");
        }
        user.setName("Aubrey"+RandomUtil.randomNumbers(4))
            .setPassword(SecureUtil.md5(user.getPassword()))
            .setAge(RandomUtil.randomInt(60));
        boolean doSave = super.save(user);
        return ResultInfo.success(doSave);
    };

    public ResultInfo<Boolean> isRegistered(String email) {
        Integer hasCount = this.count(new QueryWrapper<User>().lambda().eq(User::getMail, email));
        int i = 1/0;
        return ResultInfo.success(hasCount > 0 ? true:false);
    }
    @Override
    public ResultInfo<String> login(String email, String password) throws JOSEException {
        LambdaQueryWrapper<User> wrapper = new QueryWrapper<User>().lambda().eq(User::getMail, email);
        Integer hasCount = this.count(wrapper);
        if(hasCount <= 0){
            return ResultInfo.error("邮箱账户不存在，请前往注册！");
        }
        User user = userDao.selectOne(wrapper.eq(User::getPassword, SecureUtil.md5(password)));
        if(ObjectUtil.isEmpty(user)){
            return ResultInfo.error("密码不对，请核实！");
        }
        Date now = new Date();
        TokenInfo tokenInfo = TokenInfo.builder()
                .id(user.getId())
                .email(user.getMail())
                .iat(now.getTime())
                .exp(DateUtil.offsetDay(now, 7).getTime())
                .jti(UUID.randomUUID().toString())
                .username(user.getName())
                .authorities(null)
                .build();
        ResultInfo<String> tokenRst = jwtTokenService.generateTokenByHMAC(JSONUtil.toJsonStr(tokenInfo), ConstantUsed.JwtTokenSecret);
        redisUtil.setForTimeCustom(ConstantUsed.PrefixRedisToken+user.getMail(), tokenRst.getData(),7, TimeUnit.DAYS);
        return ResultInfo.success(tokenRst.getData());
    }
    @Override
    public ResultInfo<String> info(String token) throws ParseException, JOSEException {
        ResultInfo<TokenInfo> tokenRst = jwtTokenService.verifyTokenByHMAC(token, ConstantUsed.JwtTokenSecret);
        if(tokenRst.isSuccess()){
            User user = this.getById(tokenRst.getData().getId());
            return ResultInfo.success(user.getMail());
        }
        return ResultInfo.error("获取用户信息失败！",tokenRst.getMessage());
    }

    @Override
    public User one(String userId) {
        return super.getById(userId);
    }

    @Override
    public boolean add(User entity) {
        return super.save(entity);
    }
}
