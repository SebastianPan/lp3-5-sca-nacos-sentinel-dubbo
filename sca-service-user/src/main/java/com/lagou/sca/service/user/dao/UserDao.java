package com.lagou.sca.service.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lagou.sca.business.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends BaseMapper<User> {
}
