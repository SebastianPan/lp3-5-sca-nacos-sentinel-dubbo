package com.lagou.sca.service.user.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.lagou.sca.business.handler.GlobalHandler;
import com.lagou.sca.business.model.User;
import com.lagou.sca.business.service.CodeService;
import com.lagou.sca.common.pojo.ResultInfo;
import com.lagou.sca.service.user.service.impl.UserServiceImpl;
import com.nimbusds.jose.JOSEException;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping
public class UserController {

    @Autowired
    private UserServiceImpl userService;
    @DubboReference
    private CodeService codeService;

    @PostMapping("/send/code/{email}")
    public ResultInfo<Boolean> sendCode(@PathVariable String email) {
        return codeService.create(email);
    }


    @PostMapping("/register/{email}/{password}/{code}")
    public ResultInfo<Boolean> register(@PathVariable String email, @PathVariable String password,@PathVariable String code) {
        User user = new User().setMail(email).setPassword(password);
        if(StrUtil.isEmpty(code)){
            return ResultInfo.error("邮箱验证码不能空，请核实！");
        }
        ResultInfo<String> validateRst = codeService.validate(email, code);
        if(!validateRst.isSuccess()){
            return ResultInfo.error(validateRst.getMessage(), validateRst.getDetail());
        }
        return userService.register(user);
    }

    @SentinelResource(value = "isRegistered",
            blockHandlerClass = GlobalHandler.class, blockHandler = "handleException",
            fallbackClass = GlobalHandler.class,fallback = "handleError")
    @PostMapping("/isRegistered/{email}")
    public ResultInfo<Boolean> isRegistered(@PathVariable String email) {
        return userService.isRegistered(email);
    }

    @PostMapping("/login/{email}/{password}")
    public ResultInfo<String> login(@PathVariable String email, @PathVariable String password) throws JOSEException {
        return userService.login(email, password);
    }

    @PostMapping("/info/{token}")
    public ResultInfo<String> info(@PathVariable String token) throws ParseException, JOSEException {
        return userService.info(token);
    }

    @GetMapping("/one/{id}")
    public ResultInfo<User> one(@PathVariable String id) {
//        ResultInfo<String> validateRst = codeService.validate("728396@yeah.net", "173299");
        return ResultInfo.success(userService.getById(id));
    }

    @GetMapping("/save/{name}")
    public ResultInfo<User> save(@PathVariable String name) {
        User user = new User().setName(name);
        if(userService.save(user)){
            return ResultInfo.success(user);
        }
        return ResultInfo.error("保存失败！");
    }

    @GetMapping("/logout")
    public ResultInfo<User> logout() {
        return ResultInfo.success(null);
    }

}
