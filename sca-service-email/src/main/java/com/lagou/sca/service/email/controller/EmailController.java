package com.lagou.sca.service.email.controller;

import com.lagou.sca.business.service.EmailService;
import com.lagou.sca.common.pojo.ResultInfo;
import com.lagou.sca.service.email.service.impl.EmailServiceImpl;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class EmailController {

    @Autowired
    private EmailServiceImpl emailService;

    @PostMapping("/{email}/{code}")
    public ResultInfo<Boolean> email(@PathVariable String email, @PathVariable String code) {
        return emailService.email(email, code);
    }
}
