package com.lagou.sca.service.email.service.impl;

import com.lagou.sca.business.service.EmailService;
import com.lagou.sca.common.pojo.ResultInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import java.io.File;

@Slf4j
@DubboService
@org.springframework.stereotype.Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String defaultFrom;

    public ResultInfo<Boolean> email(String email, String code) {
        this.sendSimpleMail(email, "新的验证码", "验证码:"+code);
        return ResultInfo.success(true);
    }

    /**
     * 文本
     * @param to
     * @param subject
     * @param content
     */
    private void sendSimpleMail(String to, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(defaultFrom);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);
        try {
            mailSender.send(message);
            log.info("simple mail had send。");
        } catch (Exception e) {
            log.error("send mail error", e);
        }
    }

    /**
     * @param to
     * @param subject
     * @param content
     */
    private void sendTemplateMail(String to, String subject, String content) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            //true表示需要创建一个multipart message
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(defaultFrom);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content, true);
            mailSender.send(message);
            log.info("send template success");
        } catch (Exception e) {
            log.error("send template eror", e);
        }
    }

    /**
     * 附件
     *
     * @param to
     * @param subject
     * @param content
     * @param filePath
     */
    private void sendAttachmentsMail(String to, String subject, String content, String filePath){
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(defaultFrom);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content, true);
            FileSystemResource file = new FileSystemResource(new File(filePath));
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
            helper.addAttachment(fileName, file);
            mailSender.send(message);
            log.info("send mail with attach success。");
        } catch (Exception e) {
            log.error("send mail with attach success", e);
        }
    }

    /**
     * 发送内嵌图片
     *
     * @param to
     * @param subject
     * @param content
     * @param imgPath
     * @param imgId
     */
    private void sendInlineResourceMail(String to, String subject, String content,
                                       String imgPath, String imgId){
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(defaultFrom);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content, true);
            FileSystemResource res = new FileSystemResource(new File(imgPath));
            helper.addInline(imgId, res);
            mailSender.send(message);
            log.info("send inner resources success。");
        } catch (Exception e) {
            log.error("send inner resources fail", e);
        }
    }
}
