/*
Navicat MySQL Data Transfer

Source Server         : UCloud 117.50.22.69
Source Server Version : 80020
Source Host           : 117.50.22.69:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 80020
File Encoding         : 65001

Date: 2021-03-15 19:43:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` bigint NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `age` int DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `version` int DEFAULT NULL,
  `deleted` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'admin', 'admin', '12', '1@123.com', '1', '0');
INSERT INTO `tb_user` VALUES ('2', 'lisi', '123456', '15', '2@123.com', '1', '0');
INSERT INTO `tb_user` VALUES ('1368939514895261698', '张麻子', null, null, null, null, '0');
INSERT INTO `tb_user` VALUES ('1369637922920427521', 'Aubrey6702', 'e10adc3949ba59abbe56e057f20f883e', '12', 'Sebastian_Pan@yeah.net', null, '0');
INSERT INTO `tb_user` VALUES ('1369650139258642434', 'Aubrey0935', 'e10adc3949ba59abbe56e057f20f883e', '32', '18911147326@yeah.net', null, '0');
INSERT INTO `tb_user` VALUES ('1370626268740169729', 'Aubrey9904', 'e10adc3949ba59abbe56e057f20f883e', '57', '18812344321@yeah.net', null, '0');
INSERT INTO `tb_user` VALUES ('1371147514334629890', 'Aubrey9028', '5d93ceb70e2bf5daa84ec3d0cd2c731a', '16', '18911147327@yeah.net', null, '0');
INSERT INTO `tb_user` VALUES ('1371148630795440130', 'Aubrey4659', '5d93ceb70e2bf5daa84ec3d0cd2c731a', '46', '18911147328@yeah.net', null, '0');
