package com.lagou.sca.service.code.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.lagou.sca.business.service.CodeService;
import com.lagou.sca.business.service.EmailService;
import com.lagou.sca.common.config.ConstantUsed;
import com.lagou.sca.common.pojo.ResultInfo;
import com.lagou.sca.common.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@DubboService
@org.springframework.stereotype.Service
public class CodeServiceImpl implements CodeService {

    @Autowired
    private RedisUtil<String, Object> redisUtil;
    @Reference
    private EmailService emailService;

    public ResultInfo<Boolean> create(String email) {
        String code = RandomUtil.randomNumbers(6);
        redisUtil.setForTimeMS(ConstantUsed.PrefixRedisEmailCode +email, code, 1*60*1000);
        ResultInfo<Boolean> emailRst = emailService.email(email, code);
        return emailRst;
    }

    public ResultInfo<String> validate(String email, String code) {
        if(StrUtil.isEmpty(code) || StrUtil.isEmpty(email)){
            return ResultInfo.error("邮箱或验证码空！");
        }
        String cacheCode = redisUtil.get(ConstantUsed.PrefixRedisEmailCode + email);
        if(StrUtil.isEmpty(cacheCode)){
            return ResultInfo.error("验证码已超时失效，请重新获取！", "2");
        }else{
            if(!cacheCode.equals(code)){
                return ResultInfo.error("验证码错误,请核实重试！").setData("1");
            }
            return ResultInfo.success("0");
        }
    }
}
