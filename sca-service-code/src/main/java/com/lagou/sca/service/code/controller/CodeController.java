package com.lagou.sca.service.code.controller;

import com.lagou.sca.business.service.CodeService;
import com.lagou.sca.common.pojo.ResultInfo;
import com.lagou.sca.service.code.service.impl.CodeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class CodeController {

    @Autowired
    private CodeServiceImpl codeService;

    @PostMapping("/create/{email}")
    public ResultInfo<Boolean> create(@PathVariable String email) {
        return codeService.create(email);
    }

    @PostMapping("/validate/{email}/{code}")
    public ResultInfo<String> validate(@PathVariable String email,@PathVariable String code ) {
        return codeService.validate(email, code);
    }



}
