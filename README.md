# LP-SCA-Nacos-Sentinel-Dubbo
演示请下载视频查看
#### 介绍
1. 通过用户注册、验证码、邮箱三个微服务
2. 构建SpringCloud+Nacos+Gateway+Sentinel(Ribbon)实现Nacos集群+网关+Nginx+微服务多实例+流控降级(+实现BlockExceptionHandler/@SentinelResource)
3. JWT Token鉴权
#### 说明
- @EnableDiscoveryClient + @DubboComponentScan: 微服务可能是提供者，也可能是消费者
- @DubboReference + @org.springframework.stereotype.Service: Dubbo容器及Spring容器共用
- 通过实现BlockExceptionHandler，达到所有微服务RPC调用，流控降级的统一返回，也可根据@SentinelResource定制每个微服务返回
- 通过Maven依赖机制，将所有微服务通用yml配置统一配置在common下的config资源中，务必是bootstrap.yml
- 配置文件按照extension-configs依次载入,流控降级规则的配置则按工程名管理在配置中心
- 网关路由转发统一采用负载lb://,过滤器设置白名单、JWT鉴权、API指定ms的限流，支持URI占位符
- base工程包含了压测jmeter\集群nacos\nginx\sentinel
- 启动顺序：nacos >> sentinel >> Email >> Code >> User >> GateWay
- http://127.0.0.1/scn/user/login
- http://127.0.0.1:9762/nacos/index.html nacos nacos
- java  -jar .\sentinel-dashboard-1.8.1.jar --server.port=8888
- http://127.0.0.1:8888/ sentinel sentinel
- http://127.0.0.1:9411/zipkin
![img.png](img.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)
![img_3.png](img_3.png)
![img_4.png](img_4.png)
![img_5.png](img_5.png)