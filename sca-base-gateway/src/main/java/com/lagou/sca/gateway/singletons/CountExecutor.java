package com.lagou.sca.gateway.singletons;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 单位时间请求缓存执行器
 */
public class CountExecutor {

    public final static String prefix = "{";
    public final static String suffix = "}";
    public final static String patten = "/";

    //单例化
    private static class CountHolder{
        private final static CountExecutor instance = new CountExecutor();
    }
    public static CountExecutor getInstance(){
        return CountHolder.instance;
    }

    //<IP,<PATH, currentStamp>>
    private Map<String, Map<String, List<Long>>> countMap = new ConcurrentHashMap<>();

    public static List<String> doMatch(String path, List<String> uris) {
        if(ObjectUtil.isEmpty(uris)){
            return Collections.emptyList();
        }
        List<String> matchUris = uris.stream().filter(whiteUri -> {
            List<String> whiteUriItems = Arrays.asList(StrUtil.split(whiteUri, patten));
            List<String> pathItems = Arrays.asList(StrUtil.split(path, patten));
            Boolean isMatch = true;
            if (whiteUriItems.size() == pathItems.size()) {
                for (int i = 0; i < whiteUriItems.size(); i++) {
                    String whiteItem = whiteUriItems.get(i);
                    if (whiteItem.contains(prefix) && whiteItem.contains(suffix)) {
                        isMatch = isMatch && true;
                    } else {
                        isMatch = isMatch && whiteItem.equals(pathItems.get(i));
                    }
                    if (!isMatch) {
                        break;
                    }
                }
            } else {
                isMatch = false;
            }
            return isMatch;
        }).collect(Collectors.toList());
        return matchUris;
    }

    public void doAdd(String ip, String path, Long stamp){
        Map<String, List<Long>> pathStampMap = null;
        if(countMap.containsKey(ip)){
            pathStampMap = countMap.get(ip);
            if(pathStampMap.containsKey(path)){
                List<Long> stamps = pathStampMap.get(path);
                stamps.add(stamp);
            }else{
                List<Long> stamps = new ArrayList<>();
                stamps.add(stamp);
                pathStampMap.put(path, stamps);
            }
        }else{
            pathStampMap = new HashMap<>();
            List<Long> stamps = new ArrayList<>();
            stamps.add(stamp);
            pathStampMap.put(path, stamps);
            countMap.put(ip, pathStampMap);
        }
    }

    public void doRemove(String ip, String path, Integer ms){
        Map<String, List<Long>> pathStampsMap = countMap.get(ip);
        if(pathStampsMap.containsKey(path)){
            List<Long> stamps = pathStampsMap.get(path);
            List<Long> cloneStamps = stamps.stream().collect(Collectors.toList());
            Date offDate = DateUtil.offsetMillisecond(new Date(), -ms).toCalendar().getTime();;
            cloneStamps.stream().forEach(stamp->{
                if(DateUtil.compare(DateUtil.calendar(stamp).getTime(), offDate) < 0){
                    stamps.remove(stamp);
                }
            });
        }
    }

    public Boolean usingOutTimes(String ip, String path, Integer ms, Integer count){
        this.doRemove(ip, path, ms);
        Map<String, List<Long>> pathStampsMap = countMap.get(ip);
        if(pathStampsMap.containsKey(path)){
            return pathStampsMap.get(path).size() >= count;
        }
        return false;
    }

}
