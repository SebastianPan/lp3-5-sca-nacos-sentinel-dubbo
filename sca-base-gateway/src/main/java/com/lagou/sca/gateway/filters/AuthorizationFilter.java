package com.lagou.sca.gateway.filters;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.lagou.sca.business.model.User;
import com.lagou.sca.business.service.UserService;
import com.lagou.sca.common.config.ConstantUsed;
import com.lagou.sca.common.enums.ResultCode;
import com.lagou.sca.common.plugins.jwt.JwtTokenService;
import com.lagou.sca.common.plugins.jwt.TokenInfo;
import com.lagou.sca.common.pojo.ResultInfo;
import com.lagou.sca.common.utils.RedisUtil;
import com.lagou.sca.gateway.properties.FilterLoopApiProperties;
import com.lagou.sca.gateway.properties.FilterWhiteApiProperties;
import com.lagou.sca.gateway.singletons.CountExecutor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Component
@EnableConfigurationProperties({FilterWhiteApiProperties.class, FilterLoopApiProperties.class})
public class AuthorizationFilter implements GlobalFilter, Ordered {

    @DubboReference
    private UserService userService;
    @Autowired
    private JwtTokenService jwtTokenService;
    @Autowired
    private RedisUtil redisUtil;

    private final FilterWhiteApiProperties filterWhiteApiProperties;
    private final FilterLoopApiProperties filterLoopApiProperties;
    public AuthorizationFilter(FilterWhiteApiProperties filterWhiteApiProperties, FilterLoopApiProperties filterLoopApiProperties) {
        this.filterWhiteApiProperties = filterWhiteApiProperties;
        this.filterLoopApiProperties = filterLoopApiProperties;
    }


    @SneakyThrows
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String clientIp = request.getRemoteAddress().getHostString();

        //API 白名单过滤
        String path = request.getURI().getPath();
        List<String> matchWhiteUris = CountExecutor.doMatch(path, filterWhiteApiProperties.getUris());

        ResultInfo result = ResultInfo.error(ResultCode.UNAUTHORIZED, ResultCode.UNAUTHORIZED.getMessage(),"非法请求，请核实！");
        String token = request.getHeaders().getFirst(ConstantUsed.Header_Authorization);
        //log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GateWay AuthorizationFilter<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        log.info("PATH->{}", request.getURI());
        log.info("Authorization->{}", token);
        if(matchWhiteUris.size() > 0){
            log.info("{}->请求API->放行:{}", clientIp, matchWhiteUris.get(0));
            return chain.filter(exchange);
        }
        //Token 合法请求鉴权
        if(StrUtil.isNotEmpty(token)){
            ResultInfo<TokenInfo> tokenRst = jwtTokenService.verifyTokenByHMAC(token, ConstantUsed.JwtTokenSecret);
            if(tokenRst.isSuccess()
                && token.equals(redisUtil.get(ConstantUsed.PrefixRedisToken+tokenRst.getData().getEmail()))){
                if(CountExecutor.doMatch(path, filterLoopApiProperties.getUris()).size() == 0){
                    Long userId = tokenRst.getData().getId();
                    User user = userService.getById(userId.toString());
                    log.info("获取用户信息:{}->[{}]", ObjectUtil.isEmpty(user) ? "查无用户": userId, user.getMail());
                    if(!ObjectUtil.isEmpty(user)){
                        return chain.filter(exchange);
                    }
                    result.setMessage("查询出错啦").setDetail("查无用户ID："+ userId);
                }else{
                    return chain.filter(exchange);
                }
            }else{
                result.setMessage(!tokenRst.isSuccess() ? tokenRst.getMessage():ResultCode.UNAUTHORIZED.getMessage()).setDetail(tokenRst.getDetail());
            }
        }
        log.warn("非法请求:{}", path);
        return response.writeWith(Mono.just(response.bufferFactory().wrap(JSONUtil.toJsonStr(result).getBytes())));
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
