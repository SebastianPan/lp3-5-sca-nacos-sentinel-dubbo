package com.lagou.sca.gateway.filters;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.lagou.sca.common.enums.ResultCode;
import com.lagou.sca.common.pojo.ResultInfo;
import com.lagou.sca.common.utils.RedisUtil;
import com.lagou.sca.gateway.pojo.ApiCount;
import com.lagou.sca.gateway.properties.FilterCountApiProperties;
import com.lagou.sca.gateway.singletons.CountExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
@EnableConfigurationProperties({FilterCountApiProperties.class})
public class CountIpApiFilter implements GlobalFilter, Ordered{

    @Autowired
    private RedisUtil redisUtil;

    private final FilterCountApiProperties filterCountApiProperties;
    public CountIpApiFilter(FilterCountApiProperties filterCountApiProperties) {
        this.filterCountApiProperties = filterCountApiProperties;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String clientIp = request.getRemoteAddress().getHostString();
        String path = request.getURI().getPath();
        //log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GateWay CountApiFilter<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        //缓存IP请求PATH当前时间戳
        CountExecutor.getInstance().doAdd(clientIp, path, DateUtil.current());
        List<Map<String, String>> apiCountMap = filterCountApiProperties.getApis();
        List<ApiCount> apiCounts = new ArrayList<>();
        apiCountMap.stream().forEach(map->{
            apiCounts.add(Convert.convert(ApiCount.class, map));
        });
        Optional<ApiCount> optional = apiCounts.stream().filter(apiCount -> {
            return CountExecutor.doMatch(path, CollUtil.newArrayList(apiCount.getPath())).size() > 0;
        }).findFirst();
        Boolean isOut = false;
        if(optional.isPresent()){
            ApiCount apiCount = optional.get();
            isOut = CountExecutor.getInstance()
                    .usingOutTimes(clientIp, path, Integer.parseInt(apiCount.getMs()),
                            Integer.parseInt(apiCount.getCount()));
        }else{
            isOut = CountExecutor.getInstance()
                    .usingOutTimes(clientIp, path, filterCountApiProperties.getMs(),
                            filterCountApiProperties.getCount());
        }
        ResultInfo result = ResultInfo.error(ResultCode.MORE_REGISTER, ResultCode.MORE_REGISTER.getMessage(),"操作次数大于IP请求防爆刷阈值啦");
        if(isOut) {
            log.warn("请求被限制频繁防爆刷:{}->{}", clientIp, path);
            return response.writeWith(Mono.just(response.bufferFactory().wrap(JSONUtil.toJsonStr(result).getBytes())));
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 2;
    }
}
