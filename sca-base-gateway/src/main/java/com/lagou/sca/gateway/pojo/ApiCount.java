package com.lagou.sca.gateway.pojo;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Builder
public class ApiCount {

    //API PATH路径
    private String path;
    //单位时长(毫秒)
    private String ms;
    //单位调用次数
    private String count;

}
