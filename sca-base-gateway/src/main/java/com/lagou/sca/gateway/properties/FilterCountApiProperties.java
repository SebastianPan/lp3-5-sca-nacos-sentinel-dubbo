package com.lagou.sca.gateway.properties;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Data
@Accessors(chain = true)
@ConfigurationProperties(prefix = "filter.count")
public class FilterCountApiProperties {

    private static final ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();

    //通用单位时长(毫秒)
    private Integer ms;
    //通用单位调用次数
    private Integer count;
    //指定API Path单位时长访问次数阈值
    private List<Map<String, String>> apis;


    private Resource[] getResources(String location) {
        try {
            return resourceResolver.getResources(location);
        } catch (IOException e) {
            return new Resource[0];
        }
    }

}
